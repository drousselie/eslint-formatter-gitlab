/* eslint-disable global-require */
const path = require('path');

let env;

beforeEach(() => {
  env = Object.assign({}, process.env);
  jest.resetModules();
});

afterEach(() => {
  process.env = env;
});

describe('getOutputPath', () => {
  it('should detect the output path from the GitLab CI environment', () => {
    process.env.CI_PROJECT_DIR = process.cwd();
    process.env.CI_JOB_NAME = 'eslint';
    const { getOutputPath } = require('.');
    expect(getOutputPath()).toBe(path.join(process.cwd(), 'gl-codequality.json'));
  });

  it('should throw an error if no codequality output was detected', () => {
    process.env.CI_PROJECT_DIR = process.cwd();
    process.env.CI_CONFIG_PATH = '__fixtures__/.gitlab-ci.yml';
    process.env.CI_JOB_NAME = 'missing-code-quality';
    const { getOutputPath } = require('.');
    expect(getOutputPath).toThrow(
      'Expected missing-code-quality.artifacts.reports.codequality to be one exact path, but no value was found.',
    );
  });

  it('should throw an error if the codequality output is an array', () => {
    process.env.CI_PROJECT_DIR = process.cwd();
    process.env.CI_CONFIG_PATH = '__fixtures__/.gitlab-ci.yml';
    process.env.CI_JOB_NAME = 'array-code-quality';
    const { getOutputPath } = require('.');
    expect(getOutputPath).toThrow(
      'Expected array-code-quality.artifacts.reports.codequality to be one exact path, but found an array instead.',
    );
  });

  it('should throw an error if the codequality output is not a string', () => {
    process.env.CI_PROJECT_DIR = process.cwd();
    process.env.CI_CONFIG_PATH = '__fixtures__/.gitlab-ci.yml';
    process.env.CI_JOB_NAME = 'non-string-code-quality';
    const { getOutputPath } = require('.');
    expect(getOutputPath).toThrow(
      'Expected non-string-code-quality.artifacts.reports.codequality to be one exact path, but found 3 instead.',
    );
  });

  it('should throw an error if the codequality output is a glob', () => {
    process.env.CI_PROJECT_DIR = process.cwd();
    process.env.CI_CONFIG_PATH = '__fixtures__/.gitlab-ci.yml';
    process.env.CI_JOB_NAME = 'glob-code-quality';
    const { getOutputPath } = require('.');
    expect(getOutputPath).toThrow(
      'Expected glob-code-quality.artifacts.reports.codequality to be one exact path, but found a glob instead.',
    );
  });
});

describe('convert', () => {
  it('should convert ESLint messages into GitLab code quality messages', () => {
    const { convert } = require('.');
    const result = convert([
      {
        filePath: path.join(process.cwd(), 'filemame.js'),
        messages: [{ line: 42, message: 'This is a linting error', ruleId: 'linting-error' }],
      },
    ]);
    expect(result).toStrictEqual([
      {
        description: 'This is a linting error',
        fingerprint: '4865d2b12aa92583749538a5c966fe61',
        location: { lines: { begin: 42 }, path: 'filemame.js' },
      },
    ]);
  });
});
